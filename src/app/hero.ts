export interface Hero {
  id: number;
  name: string;
  age: number;
  gender: string;
  specific_marks: string;
  note: string;
}