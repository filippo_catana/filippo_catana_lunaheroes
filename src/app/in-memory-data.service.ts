
import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 11, name: 'Batman' },
        { id: 12, name: 'Hulk' , age: 30, gender: 'M', specific_marks: 'green', note: ''},
        { id: 13, name: 'Superman' , age: 200, gender: 'M', specific_marks: 'alien', note: ''},
        { id: 14, name: 'Spiderman' , age: 20, gender: 'M', specific_marks: 'spider', note: ''},
        { id: 15, name: 'Ironman' , age: 40, gender: 'M', specific_marks: 'tycoon', note: ''},
        { id: 16, name: 'Wonder Woman' , age: 1000, gender: 'F', specific_marks: '', note: ''},
        { id: 17, name: 'Black Panther' , age: 25, gender: 'M', specific_marks: 'king', note: ''},
        { id: 18, name: 'Wolverine' , age: 40, gender: 'M', specific_marks: 'claws', note: ''},
        { id :19, name: 'Captain America' , age: 80, gender: 'M', specific_marks: '', note: ''},
        { id: 20, name: 'Flash', age: 25, gender: 'M', specific_marks: 'speed', note: ''}
    ];
    return {heroes};
  }

  
  genId(heroes: Hero[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  }
}