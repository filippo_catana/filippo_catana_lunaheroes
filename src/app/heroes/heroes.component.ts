import { Component, OnInit } from '@angular/core';
import { Hero } from 'C:/Users/filip/lunaheroes/src/app/hero';
import { HEROES } from 'C:/Users/filip/lunaheroes/src/app/mock-heroes';
import { HeroService } from 'C:/Users/filip/lunaheroes/src/app/hero.service';
import { MessageService } from 'C:/Users/filip/lunaheroes/src/app/message.service';


@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  heroes: Hero[];

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
    .subscribe(heroes => this.heroes = heroes);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.addHero({ name } as Hero)
      .subscribe(hero => {
        this.heroes.push(hero);
      });
  }

  delete(hero: Hero): void {
    this.heroes = this.heroes.filter(h => h !== hero);
    this.heroService.deleteHero(hero).subscribe();
  }

}